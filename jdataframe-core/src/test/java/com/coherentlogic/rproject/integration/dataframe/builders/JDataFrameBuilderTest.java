package com.coherentlogic.rproject.integration.dataframe.builders;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.coherentlogic.rproject.integration.dataframe.adapters.JSONAdapter;
import com.coherentlogic.rproject.integration.dataframe.domain.JDataFrame;

/**
 * Unit test for the JDataFrameBuilder class.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class JDataFrameBuilderTest {

    @Test
    public void testToJson() {

        String actual = (String) new JDataFrameBuilder<String, Object[]>(
            new JDataFrame<String, Object[]>(),
            new JSONAdapter<String, Object[]>())
        .addColumn("Code", new Object[] {"WV", "VA", })
        .addColumn("Description", new Object[] {"West Virginia", "Virginia"})
        .serialize();

        assertEquals("{\"Description\":[\"West Virginia\",\"Virginia\"],\"Code\":[\"WV\",\"VA\"]}", actual);
    }
}

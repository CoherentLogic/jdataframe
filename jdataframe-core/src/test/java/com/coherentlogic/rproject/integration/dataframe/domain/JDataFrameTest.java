package com.coherentlogic.rproject.integration.dataframe.domain;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * Unit test for the JDataFrame class.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class JDataFrameTest {

    @Test
    public void testAddNullColumn() {

        JDataFrame dataFrame = new JDataFrame();

        assertThrows (
            NullPointerException.class,
            () -> {
                dataFrame.addColumn((Column) null);
            }
        );
    }

    @Test
    public void testAddColumnWithNullHeader() {

        Column column = new Column (null, new ArrayList<String> ());

        JDataFrame dataFrame = new JDataFrame();

        assertThrows (
            NullPointerException.class,
            () -> {
                dataFrame.addColumn(column);
            }
        );
    }
}

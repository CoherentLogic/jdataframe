package com.coherentlogic.rproject.integration.dataframe.domain;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.jupiter.api.Test;

public class ColumnTest {

    @Test
    public void foo () {

        final String targetHeader = "Some Header";

        Column<String, String> targetColumn = new Column<> (targetHeader);

        AtomicBoolean flag = new AtomicBoolean (false);

        String[] targetValues = new String[] {"Hello", "World"};

        targetColumn.addColumnListener((actionType, column, header, values) -> {

            assertEquals(targetColumn, column);
            assertEquals(targetHeader, header);
            assertEquals(targetValues, values);

            flag.set(true);
        });

        targetColumn.addValues(targetValues);
        
        assertTrue(flag.get());
    }
}

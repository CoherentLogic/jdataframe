package com.coherentlogic.rproject.integration.dataframe.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.adapters.InReturnAdapterSpecification;
import com.coherentlogic.coherent.data.adapter.core.util.WelcomeMessage;
import com.coherentlogic.rproject.integration.dataframe.adapters.ObjectArrayAdapter;
import com.coherentlogic.rproject.integration.dataframe.adapters.JSONAdapter;
import com.coherentlogic.rproject.integration.dataframe.domain.Column;
import com.coherentlogic.rproject.integration.dataframe.domain.JDataFrame;

/**
 * Facilitates representation of data in Java which can be converted into a data frame in the R Project for Statistical
 * Computing using the RSONIO package.
 *
 * <pre>
 * String json = new JDataFrameBuilder()
 *     .addColumn("Code", new Object[] {"WV", "VA"})
 *     .addColumn("Description", new Object[] {"West Virginia", "Virginia"})
 *     .serialize();
 * </pre>
 *
 * ObjectArrayAdapter Example:
 * 
 * <pre>
 *
 * </pre>
 *
 * JSONAdapter Example:
 *
 * <pre>
 * remoteGroovyScript <- paste (
 *     "@Grab(group='com.coherentlogic.rproject.integration', module='jdataframe-core', version='1.0.1-RELEASE')      ",
 *     "@Grab(group='com.coherentlogic.enterprise-data-adapter', module='data-model-core', version='3.0.3-RELEASE')   ",
 *     "import com.coherentlogic.rproject.integration.dataframe.builders.JDataFrameBuilder                            ",
 *     "JDataFrameBuilder<String> builder = new JDataFrameBuilder<String>(JDataFrameBuilder.DEFAULT_REMOTE_ADAPTER)   ",
 *     "                                                                                                              ",
 *     "def texts = [] as List                                                                                        ",
 *     "def numbers = [] as List                                                                                      ",
 *     "                                                                                                              ",
 *     "for (ctr in 0..1000000) {                                                                                     ",
 *     "    texts << new String(\"text ${ctr % 10}\")                                                                 ",
 *     "    numbers << (ctr / 5)                                                                                      ",
 *     "}                                                                                                             ",
 *     "                                                                                                              ",
 *     "return (String) builder.addColumn('Text', texts).addColumn('Number', numbers).serialize()                     ",
 *     sep="\n");
 *
 *  remoteResult <- Evaluate (groovyScript=remoteGroovyScript)
 *
 *  system.time(remoteTempDF <- RJSONIO::fromJSON(remoteResult))
 *
 * </pre>
 *
 * user  system elapsed 
 * 4.21    0.28    4.55
 *
 * uncoercedResultDF <- as.data.frame(do.call("rbind", json_file))
 * coercedResultDF <- t(uncoercedResultDF)
 * result <- as.data.frame(coercedResultDF))
 *
 * @see <a href="http://www.r-tutor.com/r-introduction/data-frame">Data Frame</a>
 * @see <a href="https://cran.r-project.org/web/packages/RJSONIO/index.html">RJSONIO</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class JDataFrameBuilder<H, R> {

    private static final Logger log = LoggerFactory.getLogger(JDataFrameBuilder.class);

    static final String[] WELCOME_MESSAGE = {
        " Coherent Logic JDataFrameBuilder version 1.0.1-RELEASE                        "
    };

    static {

        WelcomeMessage welcomeMessage = new WelcomeMessage();

        for (String next : WELCOME_MESSAGE) {
            welcomeMessage.addText(next);
        }

        welcomeMessage.display();
    }

    /**
     * The target data frame.
     */
    private final JDataFrame<H, R> dataFrame;

    private final InReturnAdapterSpecification<JDataFrame<H, R>, ?> dataFrameAdapter;

    /**
     * Default constructor uses a new {@link JDataFrame} and {@link JSONAdapter}.
     */
    public JDataFrameBuilder() {
        this(new JDataFrame<H, R> (), new JSONAdapter<H, R>());
    }

    public JDataFrameBuilder(InReturnAdapterSpecification<JDataFrame<H, R>, ?> dataFrameAdapter) {
        this(new JDataFrame<H, R> (), dataFrameAdapter);
    }

    /**
     * A constructor 
     */
    public JDataFrameBuilder(
        JDataFrame<H, R> dataFrame,
        InReturnAdapterSpecification<JDataFrame<H, R>, ?> dataFrameAdapter
    ) {
        this.dataFrame = dataFrame;
        this.dataFrameAdapter = dataFrameAdapter;
    }

    /**
     * @return The instance of JDataFrame that is assigned to this object.
     */
    public JDataFrame<H, R> getDataFrame() {
        return dataFrame;
    }

    /**
     * Adds a column to the columns map using the header as the key.
     *
     * @param header The column header, which cannot be null.
     *
     * @param values The column values.
     *
     * @return This instance of JDataFrame, which is used to facilitate method chaining.
     */
    public JDataFrameBuilder<H, R> addColumn(H header, R values) {

        Column<H, R> column = (Column<H, R>) dataFrame.getColumn(header);

        if (column == null)
            column = new Column<H, R> (header);

        column.addValues((R[]) values);

        addColumn(column);

        return this;
    }

    /**
     * Adds the column to the columns map using the header as the key.
     *
     * @param column The column to addOrReturnExistingColumn.
     *
     * @return This instance of JDataFrame, which is used to facilitate method chaining.
     */
    public JDataFrameBuilder<H, R> addColumn(Column<H, R> column) {

        dataFrame.addColumn(column);

        return this;
    }

    /**
     * @return The internal data frame as JSON text.
     */
    public Object serialize () {

        Object result = dataFrameAdapter.adapt(dataFrame);

        return result;
    }
}

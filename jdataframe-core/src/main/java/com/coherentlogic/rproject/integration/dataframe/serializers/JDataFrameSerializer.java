package com.coherentlogic.rproject.integration.dataframe.serializers;

import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.rproject.integration.dataframe.builders.JDataFrameBuilder;
import com.coherentlogic.rproject.integration.dataframe.domain.JDataFrame;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Converts all columns in the dataFrame into the appropriate Gson objects.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class JDataFrameSerializer<H, R> implements JsonSerializer<JDataFrame<H, R>> {

    private static final Logger log = LoggerFactory.getLogger(JDataFrameBuilder.class);

    @Override
    public JsonElement serialize(
        JDataFrame<H, R> dataFrame,
        Type srcType,
        JsonSerializationContext context
    ) {

        final JsonObject topLevelObject = new JsonObject ();

        dataFrame
            .getColumns()
            .forEach(
                (header, column) -> {

                    Object values = column.getValuesAsArray();

                    JsonElement serializedColumn = context.serialize(values);

                    log.debug("Adding the column with header: " + header + ", column: " + column + ", and "
                        + "serializedColumn: " + serializedColumn + " to the topLevelObject.");

                    String headerText = header.toString();

                    topLevelObject.add(headerText, serializedColumn);
                }
            );

        return topLevelObject;
    }
}

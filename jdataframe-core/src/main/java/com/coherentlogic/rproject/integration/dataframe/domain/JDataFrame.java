package com.coherentlogic.rproject.integration.dataframe.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An R-style data frame representation in Java, the purpose of which is to allow the developer the capability of
 * building a table, which can be exported as JSON and then converted into a data frame using the RJSONIO package.
 *
 * @see <a href="http://www.r-tutor.com/r-introduction/data-frame">Data Frame</a>
 * @see <a href="https://cran.r-project.org/web/packages/RJSONIO/index.html">RJSONIO</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class JDataFrame<H, R> implements Serializable {

    private static final long serialVersionUID = -8020012069039566448L;

    private final Map<H, Column<H, R>> columns;

    /**
     * The default constructor assigns a new instance of {@link java.util.HashMap} to the columns.
     */
    public JDataFrame () {
        this (new HashMap<H, Column<H, R>> ());
    }

    /**
     * Constructor assigns the instance of {@link java.util.HashMap} to the columns.
     *
     * @param columns The columns in the data frame.
     */
    public JDataFrame (Map<H, Column<H, R>> columns) {
        this.columns = columns;
    }

    /**
     * Columns getter method.
     */
    public Map<H, Column<H, R>> getColumns() {
        return columns;
    }

    /**
     * Columns getter method.
     */
    public Column<H, R> getColumn(H header) {
        return columns.get(header);
    }

    static void validate (Column<?, ?> column) {
        if (column == null)
            throw new NullPointerException("The column is null.");
        else if (column.getHeader() == null)
            throw new NullPointerException("The column header is null.");
    }

    /**
     * Method adds the column to the {@link #columns} map using the header as the key.
     *
     * @param column The column that will be added to the columns map using the header as the key.
     *
     * @return The previous column or null.
     */
    public Column<H, R> addColumn(Column<H, R> column) {

        validate (column);

        return columns.put(column.getHeader(), column);
    }

    /**
     * Method adds a column with the given header to the {@link #columns} map. If a column already exists it will be
     * returned instead of creating a new column.
     *
     * @param header The column header.
     *
     * @return The new empty column or the existing column if has already been added to the columns map.
     *
     * @todo Should we name this addOrReturnExistingColumn?
     */
    public Column<H, R> addOrReturnExistingColumn(H header) {

        Column<H, R> result = columns.get(header);

        if (result == null) {
            result = new Column<H, R> (header);
            columns.put(header, result);
        }

        return result;
    }

    /**
     * Method replaces the column to the {@link #columns} map using the header as the key.
     *
     * @param column The column that will be replaced (using the header as the key).
     *
     * @return The previous column or null.
     */
    public Column<H, R> delete (Column<H, R> column) {

        validate (column);

        return columns.remove(column.getHeader());
    }

    /**
     * 
     * @param newColumn
     *
     * @return The old column.
     */
    public Column<H, R> replace (Column<H, R> newColumn) {

        validate(newColumn);

        return columns.replace(newColumn.getHeader(), newColumn);
    }

//    public Column<H, R> merge (Column<H, R> column) {
//
//        JDataFrame.validate (column);
//
//        Column<H, R> original = columns.get(column.getHeader());
//
//        Column<H, R> newColumn = original.merge (original, column);
//
//        columns.replace(newColumn.getHeader(), newColumn);
//
//        return newColumn;
//    }

//    public <T> Column<H, R> addValues (String header, T... values) {
//
//        Column<H, R> target = columns.get(header);
//
//        target.addValues(values);
//
//        columns.replace(newColumn.getHeader(), newColumn);
//
//        return newColumn;
//    }

    public boolean exists (String header) {
        return columns.get (header) != null;
    }

    public boolean exists (Column<H, R> column) {

        validate(column);

        return columns.containsKey(column.getHeader()) && columns.containsValue(column);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((columns == null) ? 0 : columns.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JDataFrame<?, ?> other = (JDataFrame<?, ?>) obj;
        if (columns == null) {
            if (other.columns != null)
                return false;
        } else if (!columns.equals(other.columns))
            return false;
        return true;
    }

    /**
     * @return A description including the total number of rows (headers) and the total number of columns across all
     *  rows.
     */
    public String getStatistics () {

        final AtomicInteger totalColumns = new AtomicInteger (0);

        columns.forEach(
            (header, column) -> {
                totalColumns.addAndGet(column.size ());
            }
        );

        return "rows (headers): " + columns.size() + ", total number of columns across all rows: " + totalColumns;
    }

    @Override
    public String toString() {

        StringBuilder columnsText = new StringBuilder ();

        columns.forEach(
            (key, column) -> {
                columnsText.append("column: " + column).append(", ");
            }
        );

        String columnsValues = columnsText.toString();

        columnsValues = columnsValues.replaceAll(", $", "");

        return "JDataFrame [columns=" + columnsValues + "]";
    }
}

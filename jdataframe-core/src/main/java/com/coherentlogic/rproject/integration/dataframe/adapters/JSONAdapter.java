package com.coherentlogic.rproject.integration.dataframe.adapters;

import com.coherentlogic.coherent.data.adapter.core.adapters.InReturnAdapterSpecification;
import com.coherentlogic.rproject.integration.dataframe.domain.JDataFrame;
import com.coherentlogic.rproject.integration.dataframe.serializers.JDataFrameSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * An adapter that returns the dataframe as JSON.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class JSONAdapter<H, R> implements InReturnAdapterSpecification<JDataFrame<H, R>, String> {

    /**
     * @return An new instance of {@link com.google.gson.Gson} with a type adapter for the JDataFrame class assigned.
     */
    static Gson newGson () {

        GsonBuilder gsonBuilder = new GsonBuilder();

        Gson gson = gsonBuilder
            .registerTypeAdapter(JDataFrame.class, new JDataFrameSerializer())
            .create();

        return gson;
    }

    /**
     * Used to convert the {@link #dataFrame} into JSON.
     */
    private final Gson gson;

    public JSONAdapter () {
        this (newGson());
    }

    public JSONAdapter (Gson gson) {
        this.gson = gson;
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public String adapt(JDataFrame<H, R> source) {
        return gson.toJson(source);
    }
}

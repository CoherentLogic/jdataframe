package com.coherentlogic.rproject.integration.dataframe.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class MergeFailedException extends NestedRuntimeException {

    private static final long serialVersionUID = -7991007903130560460L;

    public MergeFailedException(String msg) {
        super(msg);
    }

    public MergeFailedException(String msg, Throwable cause) {
        super(msg, cause);
    }
}

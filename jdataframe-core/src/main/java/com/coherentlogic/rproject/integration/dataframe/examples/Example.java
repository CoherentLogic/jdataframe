package com.coherentlogic.rproject.integration.dataframe.examples;

import java.util.Arrays;

import com.coherentlogic.rproject.integration.dataframe.adapters.ObjectArrayAdapter;
import com.coherentlogic.rproject.integration.dataframe.builders.JDataFrameBuilder;
import com.coherentlogic.rproject.integration.dataframe.domain.Column;

/**
 * A simple example which demonstrates the JDataFrameBuilder in action.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Example {

    public static void main(String[] args) {

        JDataFrameBuilder<String, Object> jdataFrameBuilder = new JDataFrameBuilder<String, Object>(
            new ObjectArrayAdapter<String, Object>()
        );

        Column<String, Object> codeColumn = new Column<> ("Code");

        codeColumn.addColumnListener((actionType, column, header, values) -> {
            System.out.println("columnListener called with actionType: " + actionType + ", column: " + column + ", header: " + header + ", values: " + Arrays.toString(values));
        });

        codeColumn.addValues("WV", "VA");

        Object[] results = (Object[]) jdataFrameBuilder
            .addColumn(codeColumn)
            .addColumn("Description", new Object[] {"West Virginia", "Virginia"})
            .addColumn("Numbers", new Number[] {123, 456})
            .serialize();

        final StringBuilder bldr = new StringBuilder();

        for (Object result : results) {

            Object[] values = (Object[]) result;

            for (Object value : values) {
                bldr.append(value).append(",");
            }
            bldr.deleteCharAt(bldr.length() - 1).append("\n");
        }

        System.out.println(bldr.toString());

        System.exit(0);
    }
}

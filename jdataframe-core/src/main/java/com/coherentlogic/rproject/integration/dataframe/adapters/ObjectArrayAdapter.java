package com.coherentlogic.rproject.integration.dataframe.adapters;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.adapter.core.adapters.InReturnAdapterSpecification;
import com.coherentlogic.rproject.integration.dataframe.domain.JDataFrame;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ObjectArrayAdapter<H, R> implements InReturnAdapterSpecification<JDataFrame<H, R>, Object[]> {

    @Override
    public Object[] adapt(JDataFrame<H, R> source) {

        final List<Object> results = new ArrayList<Object> ();

        source
            .getColumns()
            .forEach(
                (key, column) -> {

                    final List<Object> values = new ArrayList<Object> ();

                    values.add(column.getHeader ());

                    values.addAll(column.getValues());

                    results.add(values.toArray());
                }
            );

        return results.toArray();
    }
}

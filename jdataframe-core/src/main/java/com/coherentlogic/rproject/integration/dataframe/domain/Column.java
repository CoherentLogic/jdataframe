package com.coherentlogic.rproject.integration.dataframe.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

import com.coherentlogic.rproject.integration.dataframe.exceptions.MergeFailedException;

/**
 * A column is comprised of a header and values.
 *
 * <H> The header type.
 *
 * <R> The type of data the column contains.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Column<H, R> implements Serializable {

    public enum ActionEnum {
        beforeAdd, afterAdd;
    }

    private static final long serialVersionUID = -2642348635593558144L;

    private final H header;

    private final List<R> values;

    /**
     * A constructor for a column with a header and values.
     *
     * @param header The header (or column name).
     *
     * @param values The column values.
     */
    public Column(H header) {
        this(header, new ArrayList<R> ());
    }

    /**
     * A constructor for a column with a header and values.
     *
     * @param header The header (or column name).
     *
     * @param values The column values.
     */
    public Column(H header, List<R> values) {
        this(header, new ArrayList<R> (), new ArrayList<ColumnListener<H, R>> ());
    }

    /**
     * A constructor for a column with a header and values.
     *
     * @param header The header (or column name).
     *
     * @param values The column values.
     */
    public Column(H header, List<R> values, List<ColumnListener<H, R>> columnListenerList) {
        this.header = header;
        this.values = values;
        this.columnListenerList = columnListenerList;
    }

    /**
     * @return The header (or column name).
     */
    public H getHeader() {
        return header;
    }

    /**
     * @return The column values.
     */
    public Object[] getValuesAsArray() {
        return values.toArray();
    }

    public List<R> getValues() {
        return values;
    }

//    public List<R> addValues (R value) {
//
//        Object result = new Object[] { value };
//
//        return addValues ( (R[]) result );
//    }

    public List<R> addValues (@SuppressWarnings("unchecked") R... values) {

        for (ColumnListener<H, R> nextColumnListener : columnListenerList)
            nextColumnListener.onAction(ActionEnum.beforeAdd, this, getHeader(), values);

        for (R value : values) {
            this.values.add(value);
        }

        for (ColumnListener<H, R> nextColumnListener : columnListenerList)
            nextColumnListener.onAction(ActionEnum.afterAdd, this, getHeader(), values);

        return this.values;
    }

    private final List<ColumnListener<H, R>> columnListenerList;

    public List<ColumnListener<H, R>> getColumnListenerList() {
        return columnListenerList;
    }

    public Column<H, R> addColumnListener (final ColumnListener<H, R> columnListeners) {

        List<ColumnListener<H, R>> columnListenerList = getColumnListenerList();

        List<ColumnListener<H, R>> columnListenersList = Arrays.asList(columnListeners);

        columnListenerList.addAll(columnListenersList);

        return this;
    }

    public Column<H, R> merge (Column<H, R> source, Column<H, R> target) {

        JDataFrame.validate (source);
        JDataFrame.validate (target);

        H sourceHeader = source.getHeader();
        H targetHeader = target.getHeader();

        if (!sourceHeader.equals(targetHeader))
            throw new MergeFailedException ("The source header (" + sourceHeader + ") is not equal to the target "
                + "header (" + targetHeader + ").");

        Column<H, R> newColumn = new Column<H, R> (sourceHeader, source.getValues());

        newColumn.getValues().addAll(target.getValues());

        return newColumn;
    }

    /**
     * @return Zero when values is null otherwise values.size.
     */
    public int size () {
        return (getValues() == null) ? 0 : getValues().size();
    }

    

    @Override
    public String toString() {

        final StringBuilder resultBuilder = new StringBuilder ();

        values.forEach(
            value -> {
                resultBuilder.append(value).append(", ");
            }
        );

        String resultText = resultBuilder.toString();

        resultText = resultText.replaceAll(", $", "");

        return "Column [header=" + header + ", values=" + resultText + "]";
    }
}

package com.coherentlogic.rproject.integration.dataframe.domain;

/**
 * Called before and after values are added to a column.
 *
 * @param <H> The header type.
 * @param <R> The type of data the column contains.
 */
@FunctionalInterface
public interface ColumnListener<H, R> {

    /**
     * Listener method which is called before and after values are added to a column.
     *
     * @param actionType Before or after.
     * @param column The target column.
     * @param header The column header.
     * @param values The values being added to the column.
     */
    void onAction (Column.ActionEnum actionType, Column<H, R> column, H header, R[] values);
}
